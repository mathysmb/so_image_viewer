#pragma once

#include <QtWidgets/QMainWindow>
//#include <QtWidgets>
#include "ui_ImageViewer.h"
#include "ViewerWidget.h"
#include "NewImageDialog.h"

enum filter_type
{
	linear_heat,
	Perona_Malik
};


class ImageViewer : public QMainWindow
{
	Q_OBJECT

public:
	ImageViewer(QWidget* parent = Q_NULLPTR);

private:
	Ui::ImageViewerClass* ui;
	NewImageDialog* newImgDialog;

	QSettings settings;
	QMessageBox msgBox;

	filter_type filterType;

	//ViewerWidget functions
	ViewerWidget* getViewerWidget(int tabId);
	ViewerWidget* getCurrentViewerWidget();

	//Event filters
	bool eventFilter(QObject* obj, QEvent* event);

	//ViewerWidget Events
	bool ViewerWidgetEventFilter(QObject* obj, QEvent* event);
	void ViewerWidgetMouseButtonPress(ViewerWidget* w, QEvent* event);
	void ViewerWidgetMouseButtonRelease(ViewerWidget* w, QEvent* event);
	void ViewerWidgetMouseMove(ViewerWidget* w, QEvent* event);
	void ViewerWidgetLeave(ViewerWidget* w, QEvent* event);
	void ViewerWidgetEnter(ViewerWidget* w, QEvent* event);
	void ViewerWidgetWheel(ViewerWidget* w, QEvent* event);

	//ImageViewer Events
	void closeEvent(QCloseEvent* event);

	//Image functions
	void openNewTabForImg(ViewerWidget* vW);
	bool openImage(QString filename);
	bool saveImage(QString filename);
	bool clearImage();
	bool invertColors();
	bool makeBorders(int size);
	QImage makeMirroredImage(QImage* image, int size);
	QImage cutImage(QImage img, int borderSize);
	bool makeFullStretchedImage();
	bool loadMask(QString filename);
	bool applyMask(std::vector<std::vector<double>> mask, double weight);

	// Linear heat
	bool explicitLinearHeat(double tau, int N, int saveEvery);
	bool implicitLinearHeat(double tau, int N, int saveEvery, double omega);
	std::vector<double> LHE_SOR(std::vector<double> u, double pCoeff, double qCoeff, int width, int height, int row, double omega);

	// Perona-Malik
	bool explicitPeronaMalik(double tau, int N, int saveEvery, double k);
	bool semi_implicitPeronaMalik(double tau, int N, int saveEvery, double omega, double k);
	double g(double s, double K);
	double* gradInDir(std::vector<double> data, int i, int j, std::string dir);
	double norm(double* v, int dim);
	
	bool ISODATA();

	//Inline functions
	inline bool isImgOpened() { return ui->tabWidget->count() == 0 ? false : true; }


private slots:
	//Tabs slots
	void on_tabWidget_tabCloseRequested(int tabId);
	void on_actionRename_triggered();

	//Image slots
	void on_actionNew_triggered();
	void newImageAccepted();
	void on_actionOpen_triggered();
	void on_actionSave_as_triggered();
	void on_actionClear_triggered();
	void on_actionInvert_colors_triggered();
	void on_actionMakeBorder_triggered();
	void on_actionHistogram_triggered();
	void on_actionHistogram_stretch_triggered();
	void on_actionConvolution_triggered();
	void on_actionISODATA_2_triggered();
	void on_actionBernsen_triggered();
	void on_actionLinear_Heat_triggered();
	void on_actionPerona_Malik_triggered();

	void CalculateExplicitLinearHeat();
	void ChangeImageFromBuffer(int index);
	void HideLinearHeatGroupBox();
	void ImageWidgetTabChanged(int index);
	void ExplicitRadioButton_clicked();
	void ImplicitRadioButton_clicked();

};
