#include   "ViewerWidget.h"
#include <iostream>

ViewerWidget::ViewerWidget(QString viewerName, QSize imgSize, QWidget* parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	setMouseTracking(true);
	name = viewerName;
	if (imgSize != QSize(0, 0)) {
		img = new QImage(imgSize, QImage::Format_ARGB32);
		img->fill(Qt::white);
		resizeWidget(img->size());
		setPainter();
		setDataPtr();
	}
}
ViewerWidget::~ViewerWidget()
{
	delete painter;
	delete img;
	filterImageBuffer.clear();
}
void ViewerWidget::resizeWidget(QSize size)
{
	this->resize(size);
	this->setMinimumSize(size);
	this->setMaximumSize(size);
}


#pragma region Image functions

bool ViewerWidget::setImage(const QImage& inputImg)
{
	if (img != nullptr) {
		painter->end();
		delete img;
	}
	img = new QImage(inputImg);
	if (!img) {
		return false;
	}
	resizeWidget(img->size());
	setPainter();
	setDataPtr();
	return true;
}
void ViewerWidget::setPixel(QImage *image, const int& x, const int& y, const uchar& r, const uchar& g, const uchar& b, const uchar& a)
{
	uchar* dat = image->bits();

	if (isInside(image, x, y)) {
		int startbyte = y * image->bytesPerLine() + x * 4;
		dat[startbyte] = r;
		dat[startbyte + 1] = g;
		dat[startbyte + 2] = b;
		dat[startbyte + 3] = a;
	}
}
void ViewerWidget::setPixel(QImage* image, const int& x, const int& y, const uchar& val)
{
	uchar* dat = image->bits();

	if (isInside(image, x, y)) {
		dat[y * image->bytesPerLine() + x] = val;
	}
}
void ViewerWidget::setPixel(QImage* image, const int& x, const int& y, double& val)
{
	if (isInside(image, x, y)) {
		if (val > 1) val = 1;
		if (val < 0) val = 0;
		setPixel(image, x, y, static_cast<uchar>(255 * val));
	}
}
void ViewerWidget::setPixel(QImage* image, const int& x, const int& y,  double& valR,  double& valG,  double& valB,const double& valA)
{
	if (isInside(image, x, y)) {
		if (valR > 1) valR = 1;
		if (valG > 1) valG = 1;
		if (valB > 1) valB = 1;
		

		if (valR < 0) valR = 0;
		if (valG < 0) valG = 0;
		if (valB < 0) valB = 0;
		
		double newValA = valA;
		if (newValA > 1) newValA = 1;
		if (newValA < 0) newValA = 0;
		setPixel(image, x, y, static_cast<uchar>(255 * valR), static_cast<uchar>(255 * valG), static_cast<uchar>(255 * valB), static_cast<uchar>(255 * valA));
	}
}
double ViewerWidget::intensitySum()
{
	if (data == nullptr) return -1;

	int row = getImage()->bytesPerLine();
	int depth = getImage()->depth();

	double sum = 0;

	for (int i = 0; i < getImgHeight(); i++)
	{
		for (int j = 0; j < getImgWidth(); j++)
		{
			if (depth == 8)
			{
				sum += (double)data[i * row + j];
			}
		}
	}
}

#pragma endregion

//Draw functions
void ViewerWidget::freeDraw(const QPoint& end, const QPen& pen)
{
	painter->setPen(pen);
	painter->drawLine(freeDrawBegin, end);
	update();
}

#pragma region LinearHeatFilter

void ViewerWidget::setImageFromBuffer(int index)
{
	QImage * img = getImage();
	
	int row = img->bytesPerLine();

	for (int i = 0; i < getImgHeight(); i++)
	{
		for (int j = 0; j < getImgWidth(); j++)
		{
			if (img->depth() == 8) {
				setPixel(img, j, i, static_cast<uchar>(std::min(std::max(filterImageBuffer[index][i * row + j], 0.), 255.)));

			}
			else {
				setPixel(img, j, i,
					static_cast<uchar>(std::min(std::max(filterImageBuffer[index][i * row + j * 4], 0.), 255.)),
					static_cast<uchar>(std::min(std::max(filterImageBuffer[index][i * row + j * 4 + 1], 0.), 255.)),
					static_cast<uchar>(std::min(std::max(filterImageBuffer[index][i * row + j * 4 + 2], 0.), 255.)));
			}
		}
	}
}

double ViewerWidget::intensitySumFromBuffer(int index)
{
	double sum = 0.;

	for (int i = 0; i < filterImageBuffer[index].size(); i++)
	{
		sum += filterImageBuffer[index][i];
	}

	return sum;
}


#pragma endregion

#pragma region  Histogram functions
bool ViewerWidget::calculateHistogram()
{
	if (data == nullptr) return false;

	int row = getImage()->bytesPerLine();
	int depth = getImage()->depth();

	if (depth == 8)
	{
		initializeHistBlack();
	}
	else
	{
		initializeHistColors();
	}

	for (int i = 0; i < getImgHeight(); i++)
	{
		for (int j = 0; j < getImgWidth(); j++)
		{
			if (depth == 8)
			{
				hist_black[data[i * row + j]]++;
			}
			else
			{
				hist_red[data[i * row + j * 4]]++;
				hist_green[data[i * row + j * 4 + 1]]++;
				hist_blue[data[i * row + j * 4 + 2]]++;
			}
		}
	}

	HistCalculated();

	return true;
}
double ViewerWidget::isodataTreshold()
{
	if (!isHistCalculated()) calculateHistogram();

	int K = 256;
	double q = histogramMean(0, K - 1);
	double q1 = -1;
	double n0;
	double n1;
	double mi0;
	double mi1;

	while (q != q1)
	{
		n0 = histogramCount(0, q);
		n1 = histogramCount(q + 1, K - 1);

		if (n0 == 0 || n1 == 0)
		{
			return -1;
		}

		mi0 = histogramMean(0, q);
		mi1 = histogramMean(q + 1, K - 1);

		q1 = q;
		q = (mi0 + mi1) / 2;
	}

	return q;
}
void ViewerWidget::bernsenTreshold(int r, int cmin, std::string bg)
{
	int K = 256;
	int M = img->width();
	int N = img->height();

	double** Q = new double*[M];
	for (int i = 0; i < M; i++)
	{
		Q[i] = new double[N];
	}

	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < N; j++)
		{
			if (bg == "dark")
			{
				Q[i][j] = K;
			}
			else
			{
				Q[i][j] = 0;
			}
		}
	}

	int row = img->bytesPerLine();

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			double min = data[i * row + j];
			double max = data[i * row + j];


			for (int x = j - r; x <= j + r; x++)
			{
				for (int y = i - r; y <= i + r; y++)
				{
					if (data[y * row + x] < min) min = data[y * row + x];
					if (data[y * row + x] > max) max = data[y * row + x];
				}
			}

			double c = max - min;

			Q[j][i] = (c >= cmin) ? (min + max) / 2 : Q[j][i];
		}
	}

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < M; j++)
		{
			if (data[i * row + j] < Q[j][i])
			{
				setPixel(img, j, i, static_cast<uchar>(0));
			}
			else
			{
				setPixel(img, j, i, static_cast<uchar>(255));
			}
			
		}
	}
}
int ViewerWidget::histogramCount(int a, int b)
{
	if (a < 0 || a > 255) return -1;
	if (b < 0 || b > 255) return -1;
	if (!histCalculated) return -1;

	int count = 0;
	for (int i = a; i <= b; i++)
	{
		count += hist_black[i];
	}
	return count;
}
double ViewerWidget::histogramMean(int a, int b)
{
	if (a < 0 || a > 255) return -1;
	if (b < 0 || b > 255) return -1;
	if (!histCalculated) return -1;

	double cit = 0;
	double men = 0;
	for (int i = a; i <= b; i++)
	{
		cit += i * hist_black[i];
		men += hist_black[i];
	}

	return cit / men;
}
void ViewerWidget::initializeHistBlack()
{
	hist_black = new int[256];

	for (int i = 0; i < 256; i++) 
	{
		hist_black[i] = 0;
	}
}
void ViewerWidget::initializeHistColors()
{
	hist_red = new int[256];
	hist_green = new int[256];
	hist_blue = new int[256];

	for (int i = 0; i < 256; i++) 
	{
		hist_red[i] = 0;
		hist_green[i] = 0;
		hist_blue[i] = 0;
	}
}
uchar ViewerWidget::Min(uchar* values)
{
	if (values == nullptr) return -1;

	uchar min = values[0];
	for (int i = 1; i < 256; i++)
	{
		if (values[i] < min) min = values[i];
	}

	return min;
}
uchar ViewerWidget::Max(uchar* values)
{
	if (values == nullptr) return -1;

	uchar max = values[0];
	for (int i = 1; i < 256; i++)
	{
		if (values[i] > max) max = values[i];
	}

	return max;
}

#pragma endregion

//Slots
void ViewerWidget::paintEvent(QPaintEvent* event)
{
	QPainter painter(this);
	QRect area = event->rect();
	painter.drawImage(area, *img, area);
}