#include "ImageViewer.h"
#include <iostream>

ImageViewer::ImageViewer(QWidget* parent)
	: QMainWindow(parent), ui(new Ui::ImageViewerClass)
{
	ui->setupUi(this);

	ui->linearHeatGroupBox->setVisible(false);
}

//ViewerWidget functions
ViewerWidget* ImageViewer::getViewerWidget(int tabId)
{
	QScrollArea* s = static_cast<QScrollArea*>(ui->tabWidget->widget(tabId));
	if (s) {
		ViewerWidget* vW = static_cast<ViewerWidget*>(s->widget());
		return vW;
	}
	return nullptr;
}
ViewerWidget* ImageViewer::getCurrentViewerWidget()
{
	return getViewerWidget(ui->tabWidget->currentIndex());
}

// Event filters
bool ImageViewer::eventFilter(QObject* obj, QEvent* event)
{
	if (obj->objectName() == "ViewerWidget") {
		return ViewerWidgetEventFilter(obj, event);
	}
	return false;
}

//ViewerWidget Events
bool ImageViewer::ViewerWidgetEventFilter(QObject* obj, QEvent* event)
{
	ViewerWidget* w = static_cast<ViewerWidget*>(obj);

	if (!w) {
		return false;
	}

	if (event->type() == QEvent::MouseButtonPress) {
		ViewerWidgetMouseButtonPress(w, event);
	}
	else if (event->type() == QEvent::MouseButtonRelease) {
		ViewerWidgetMouseButtonRelease(w, event);
	}
	else if (event->type() == QEvent::MouseMove) {
		ViewerWidgetMouseMove(w, event);
	}
	else if (event->type() == QEvent::Leave) {
		ViewerWidgetLeave(w, event);
	}
	else if (event->type() == QEvent::Enter) {
		ViewerWidgetEnter(w, event);
	}
	else if (event->type() == QEvent::Wheel) {
		ViewerWidgetWheel(w, event);
	}

	return QObject::eventFilter(obj, event);
}
void ImageViewer::ViewerWidgetMouseButtonPress(ViewerWidget* w, QEvent* event)
{
	QMouseEvent* e = static_cast<QMouseEvent*>(event);
	if (e->button() == Qt::LeftButton) {
		w->setFreeDrawBegin(e->pos());
		w->setFreeDrawActivated(true);
	}
}
void ImageViewer::ViewerWidgetMouseButtonRelease(ViewerWidget* w, QEvent* event)
{
	QMouseEvent* e = static_cast<QMouseEvent*>(event);
	if (e->button() == Qt::LeftButton && w->getFreeDrawActivated()) {
		w->freeDraw(e->pos(), QPen(Qt::red));
		w->setFreeDrawActivated(false);
	}
}
void ImageViewer::ViewerWidgetMouseMove(ViewerWidget* w, QEvent* event)
{
	QMouseEvent* e = static_cast<QMouseEvent*>(event);
	if (e->buttons() == Qt::LeftButton && w->getFreeDrawActivated()) {
		w->freeDraw(e->pos(), QPen(Qt::red));
		w->setFreeDrawBegin(e->pos());
	}
}
void ImageViewer::ViewerWidgetLeave(ViewerWidget* w, QEvent* event)
{
}
void ImageViewer::ViewerWidgetEnter(ViewerWidget* w, QEvent* event)
{
}
void ImageViewer::ViewerWidgetWheel(ViewerWidget* w, QEvent* event)
{
	QWheelEvent* wheelEvent = static_cast<QWheelEvent*>(event);
}

//ImageViewer Events
void ImageViewer::closeEvent(QCloseEvent* event)
{
	if (QMessageBox::Yes == QMessageBox::question(this, "Close Confirmation", "Are you sure you want to exit?", QMessageBox::Yes | QMessageBox::No))
	{
		event->accept();
	}
	else {
		event->ignore();
	}
}

#pragma region Image functions

void ImageViewer::openNewTabForImg(ViewerWidget* vW)
{
	QScrollArea* scrollArea = new QScrollArea;
	scrollArea->setObjectName("QScrollArea");
	scrollArea->setWidget(vW);

	scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidgetResizable(true);
	scrollArea->installEventFilter(this);
	scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

	vW->setObjectName("ViewerWidget");
	vW->installEventFilter(this);

	QString name = vW->getName();

	ui->tabWidget->addTab(scrollArea, name);
}
bool ImageViewer::openImage(QString filename)
{
	QFileInfo fi(filename);

	QString name = fi.baseName();
	openNewTabForImg(new ViewerWidget(name, QSize(0, 0)));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);

	ViewerWidget* w = getCurrentViewerWidget();

	QImage loadedImg(filename);
	return w->setImage(loadedImg);
}
bool ImageViewer::saveImage(QString filename)
{
	QFileInfo fi(filename);
	QString extension = fi.completeSuffix();
	ViewerWidget* w = getCurrentViewerWidget();

	QImage* img = w->getImage();
	return img->save(filename, extension.toStdString().c_str());
}
bool ImageViewer::clearImage()
{
	ViewerWidget* w = getCurrentViewerWidget();
	w->clear();
	w->update();
	return true;
}
bool ImageViewer::invertColors()
{
	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();

	int row = w->getImage()->bytesPerLine();
	int depth = w->getImage()->depth();

	for (int i = 0; i < w->getImgHeight(); i++)
	{
		for (int j = 0; j < w->getImgWidth(); j++)
		{
			if (depth == 8) {
				w->setPixel(w->getImage(), j, i, static_cast<uchar>(255 - data[i * row + j]));
			}
			else {
				uchar r = static_cast<uchar>(255 - data[i * row + j * 4]);
				uchar g = static_cast<uchar>(255 - data[i * row + j * 4 + 1]);
				uchar b = static_cast<uchar>(255 - data[i * row + j * 4 + 2]);
				w->setPixel(w->getImage(), j, i, r, g, b);
			}
		}
	}
	w->update();
	return true;
}
bool ImageViewer::makeBorders(int size)
{
	ViewerWidget* w = getCurrentViewerWidget();

	w->setImage(makeMirroredImage(w->getImage(), size));

	w->update();

	return true;
}
QImage ImageViewer::makeMirroredImage(QImage* image, int size)
{
	ViewerWidget* w = getCurrentViewerWidget();

	uchar* data = image->bits();

	int row = image->bytesPerLine();
	int depth = image->depth();

	QImage* newImg;

	if (depth == 8)
	{
		newImg = new QImage(QSize(image->width() + 2 * size, image->height() + 2 * size), QImage::Format_Grayscale8);
	}
	else
	{
		newImg = new QImage(QSize(image->width() + 2 * size, image->height() + 2 * size), QImage::Format_ARGB32);
	}

	// Copy image
	for (int i = 0; i < image->height(); i++)
	{
		for (int j = 0; j < image->width(); j++)
		{
			if (depth == 8)
			{
				w->setPixel(newImg, size + j, size + i, data[i * row + j]);
			}
			else
			{
				w->setPixel(newImg, size + j, size + i,
					data[i * row + j * 4],
					data[i * row + j * 4 + 1],
					data[i * row + j * 4 + 2]);
			}
		}
	}

	// Top and bottom border
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < image->width(); j++)
		{
			if (depth == 8)
			{
				w->setPixel(newImg, size + j, size - i - 1, data[i * row + j]);
				w->setPixel(newImg, size + j, size + w->getImgHeight() + i, data[(w->getImgHeight() - i - 1) * row + j]);
			}
			else
			{
				w->setPixel(newImg, size + j, size - i - 1,
					data[i * row + j * 4],
					data[i * row + j * 4 + 1],
					data[i * row + j * 4 + 2]);

				w->setPixel(newImg, size + j, size + w->getImgHeight() + i,
					data[(w->getImgHeight() - i - 1) * row + j * 4],
					data[(w->getImgHeight() - i - 1) * row + j * 4 + 1],
					data[(w->getImgHeight() - i - 1) * row + j * 4 + 2]);
			}
		}
	}

	// Left and right border
	for (int i = 0; i < w->getImgHeight(); i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (depth == 8)
			{
				w->setPixel(newImg, size - j - 1, size + i, data[i * row + j]);
				w->setPixel(newImg, size + w->getImgWidth() + j, size + i, data[i * row + w->getImgWidth() - j - 1]);
			}
			else
			{
				w->setPixel(newImg, size - j - 1, size + i,
					data[i * row + j * 4],
					data[i * row + j * 4 + 1],
					data[i * row + j * 4 + 2]);

				w->setPixel(newImg, size + w->getImgWidth() + j, size + i,
					data[i * row + (w->getImgWidth() - j - 1) * 4],
					data[i * row + (w->getImgWidth() - j - 1) * 4 + 1],
					data[i * row + (w->getImgWidth() - j - 1) * 4 + 2]);
			}
		}
	}

	// Corners
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (depth == 8)
			{
				// Top left
				w->setPixel(newImg, size - j - 1, size - i - 1, data[i * row + j]);

				//Top right
				w->setPixel(newImg, size + w->getImgWidth() + j, size - i - 1, data[i * row + w->getImgWidth() - j - 1]);

				// Bottom left
				w->setPixel(newImg, size - j - 1, size + w->getImgHeight() + i, data[(w->getImgHeight() - i - 1) * row + j]);

				// Bottom right
				w->setPixel(newImg, size + w->getImgWidth() + j, size + w->getImgHeight() + i,
					data[(w->getImgHeight() - i - 1) * row + w->getImgWidth() - j - 1]);

			}
			else
			{
				// Top left
				w->setPixel(newImg, size - j - 1, size - i - 1,
					data[i * row + j * 4],
					data[i * row + j * 4 + 1],
					data[i * row + j * 4 + 2]);

				//Top right
				w->setPixel(newImg, size + w->getImgWidth() + j, size - i - 1,
					data[i * row + (w->getImgWidth() - j - 1) * 4],
					data[i * row + (w->getImgWidth() - j - 1) * 4 + 1],
					data[i * row + (w->getImgWidth() - j - 1) * 4 + 2]);

				// Bottom left
				w->setPixel(newImg, size - j - 1, size + w->getImgHeight() + i,
					data[(w->getImgHeight() - i - 1) * row + j * 4],
					data[(w->getImgHeight() - i - 1) * row + j * 4 + 1],
					data[(w->getImgHeight() - i - 1) * row + j * 4 + 2]);

				// Bottom right
				w->setPixel(newImg, size + w->getImgWidth() + j, size + w->getImgHeight() + i,
					data[(w->getImgHeight() - i - 1) * row + (w->getImgWidth() - j - 1) * 4],
					data[(w->getImgHeight() - i - 1) * row + (w->getImgWidth() - j - 1) * 4 + 1],
					data[(w->getImgHeight() - i - 1) * row + (w->getImgWidth() - j - 1) * 4 + 2]);
			}
		}
	}

	return *newImg;
}
QImage ImageViewer::cutImage(QImage img, int borderSize)
{
	QImage* newImg;

	newImg = new QImage(QSize(img.width() - 2 * borderSize, img.height() - 2 * borderSize), img.format());

	for (int i = borderSize; i < newImg->height(); i++)
	{
		for (int j = borderSize; j < newImg->width(); j++)
		{

		}
	}

	return QImage();
}
bool ImageViewer::makeFullStretchedImage()
{
	ViewerWidget* w = getCurrentViewerWidget();
	uchar* data = w->getData();

	if (!w->isHistCalculated()) w->calculateHistogram();

	int row = w->getImage()->bytesPerLine();
	int depth = w->getImage()->depth();

	uchar minBlack = 0;
	uchar maxBlack = 0;
	uchar minRed = 0;
	uchar maxRed = 0;
	uchar minGreen = 0;
	uchar maxGreen = 0;
	uchar minBlue = 0;
	uchar maxBlue = 0;

	if (depth == 8)
	{
		minBlack = maxBlack = data[0];
	}
	else
	{
		minRed = maxRed = data[0];
		minGreen = maxGreen = data[1];
		minBlue = maxBlue = data[2];
	}


	for (int i = 0; i < w->getImgHeight(); i++)
	{
		for (int j = 0; j < w->getImgWidth(); j++)
		{
			if (depth == 8)
			{
				if (data[i * row + j] > maxBlack) maxBlack = data[i * row + j];
				if (data[i * row + j] < minBlack) minBlack = data[i * row + j];
			}
			else
			{
				if (data[i * row + j * 4] > maxRed) maxRed = data[i * row + j * 4];
				if (data[i * row + j * 4] < minRed) minRed = data[i * row + j * 4];

				if (data[i * row + j * 4 + 1] > maxGreen) maxGreen = data[i * row + j * 4 + 1];
				if (data[i * row + j * 4 + 1] < minGreen) minGreen = data[i * row + j * 4 + 1];

				if (data[i * row + j * 4 + 2] > maxBlue) maxBlue = data[i * row + j * 4 + 2];
				if (data[i * row + j * 4 + 2] < minBlue) minBlue = data[i * row + j * 4 + 2];
			}
		}
	}


	//ViewerWidget* newImg;
	//std::memcpy(newImg, w, sizeof(ViewerWidget));

	for (int i = 0; i < w->getImgHeight(); i++)
	{
		for (int j = 0; j < w->getImgWidth(); j++)
		{
			if (depth == 8)
			{
				w->setPixel(w->getImage(), j, i, static_cast<uchar>((255.0 / (maxBlack - minBlack)) * (data[i * row + j] - minBlack)));
			}
			else
			{
				uchar r = static_cast<uchar>((255.0 / (maxRed - minRed)) * (data[i * row + j * 4] - minRed));
				uchar g = static_cast<uchar>((255.0 / (maxGreen - minGreen)) * (data[i * row + j * 4 + 1] - minGreen));
				uchar b = static_cast<uchar>((255.0 / (maxBlue - minBlue)) * (data[i * row + j * 4 + 2] - minBlue));
				w->setPixel(w->getImage(), j, i, r, g, b);

				//std::cout << data[i * row + j * 4] << std::endl;
			}
		}
	}

	w->update();
	return true;
}
bool ImageViewer::loadMask(QString filename)
{
	QFile file(filename);

	if (!file.open(QIODevice::ReadOnly))
	{
		return false;
	}

	std::vector<std::vector<double>> mask;
	double weight = file.readLine().toDouble();

	while (!file.atEnd())
	{
		QString line = file.readLine();

		QStringList list = line.split(" ", QString::SkipEmptyParts);

		std::vector<double> lineDouble;

		for (int i = 0; i < list.count(); i++)
		{
			lineDouble.push_back(list[i].toDouble());
		}

		mask.push_back(lineDouble);
	}

	//for (int i = 0; i < mask.size(); i++)
	//{
	//	for (int j = 0; j < mask[i].size(); j++)
	//	{
	//		std::cout << mask[i][j] << " ";
	//	}
	//	std::cout << std::endl;
	//}

	ViewerWidget* w = getCurrentViewerWidget();

	//std::cout << "Stredna intenzita: " << w->intensitySum() / (w->getImage()->width() * w->getImage()->height()) << std::endl;

	applyMask(mask, weight);

	//std::cout << "Stredna intenzita: " << w->intensitySum() / (w->getImage()->width() * w->getImage()->height()) << std::endl;

	return true;
}
bool ImageViewer::applyMask(std::vector<std::vector<double>> mask, double weight)
{
	int maskSize = mask.size();

	ViewerWidget* w = getCurrentViewerWidget();
	int depth = w->getImage()->depth();

	int borderSize = (maskSize - 1) / 2;

	QImage image = makeMirroredImage(w->getImage(), borderSize);

	//QImage image = *w->getImage();
	uchar* data = image.bits();

	int row = image.bytesPerLine();
	
	double* value;
	if (depth == 8)
	{
		value = new double;
	}
	else
	{
		value = new double[3];
	}

	for (int i = borderSize; i < w->getImgHeight() + borderSize; i++)
	{
		for (int j = borderSize; j < w->getImgWidth() + borderSize; j++)
		{
			if (depth == 8)
			{
				*value = 0.;
			}
			else
			{
				value[0] = value[1] = value[2] = 0.;
			}

			for (int mi = i - borderSize; mi <= i + borderSize; mi++)
			{
				for (int mj = j - borderSize; mj <= j + borderSize; mj++)
				{
					int maski = mi - i + borderSize;
					int maskj = mj - j + borderSize;

					//std::cout << mj << " " << mi << std::endl;
					//std::cout << maskj << " " << maski << std::endl;

					if (depth == 8)
					{
						value[0] += (double)data[mi * row + mj] * mask[maskj][maski];
					}
					else
					{
						value[0] += (double)data[mi * row + mj * 4] * mask[maskj][maski];
						value[1] += (double)data[mi * row + mj * 4 + 1] * mask[maskj][maski];
						value[2] += (double)data[mi * row + mj * 4 + 2] * mask[maskj][maski];
					}

					//std::cout << mj << " " << mi << " - " << mask[maskj][maski] << std::endl;
					//std::cout << (int)value[0] << " " << (int)data[i * row + j] << std::endl;
				}
				//std::cout << std::endl;
			}

			//std::cout << std::endl;

			if (depth == 8)
			{
				w->setPixel(w->getImage(), j - borderSize, i - borderSize, static_cast<uchar>(std::min(std::max(*value * weight, 0.), 255.)));

			}
			else
			{
				w->setPixel(w->getImage(), j - borderSize, i - borderSize,
					static_cast<uchar>(std::min(std::max(value[0] * weight, 0.), 255.)),
					static_cast<uchar>(std::min(std::max(value[1] * weight, 0.), 255.)),
					static_cast<uchar>(std::min(std::max(value[2] * weight, 0.), 255.)));

				//std::cout << i << " " << j << std::endl;
			}
		}
	}

	delete[] value;
	w->update();

	return true;
}

// LINEAR HEAT EQUATION
bool ImageViewer::explicitLinearHeat(double tau, int N, int saveEvery)
{
	double h = 1;

	ViewerWidget* w = getCurrentViewerWidget();
	int depth = w->getImage()->depth();
	int row = w->getImage()->bytesPerLine();

	w->clearImageBuffer();

	// VLOZIM ORIGINALNY OBRAZOK
	std::vector<double> origin;
	uchar* originUCH = w->getData();
	for (int i = 0; i < w->height() * w->width(); i++)
	{
		origin.push_back(originUCH[i]);
	}
	w->filterBufferAddImage(origin);

	//std::cout << w->intensitySum() << std::endl;

	std::vector<double> imgDoublesLast = w->getLastImg();
	std::vector<double> imgDoublesNew(imgDoublesLast.size(), 0.0);

	for (int n = 0; n < N; n++)
	{
		double pCoeff = 1.0 - tau / (h * h) * 4.0;
		double qCoeff = tau / (h * h);

		double* value;
		if (depth == 8)
		{
			value = new double;
			*value = 0.;
		}
		else
		{
			value = new double[3];
			value[0] = value[1] = value[2] = 0.;
		}

		for (int i = 0; i < w->getImgHeight(); i++)
		{
			for (int j = 0; j < w->getImgWidth(); j++)
			{

				// INDEXY "MASKY" 
				int in = (i + 1 == w->getImgHeight()) ? i : i + 1;
				int is = (i - 1 == -1) ? 0 : i - 1;
				int je = (j + 1 == w->getImgWidth()) ? j : j + 1;
				int jw = (j - 1 == -1) ? 0 : j - 1;

				if (depth == 8)
				{
					int p = i * row + j;

					*value = pCoeff * imgDoublesLast[i * row + j] +
						qCoeff * imgDoublesLast[in * row + j] +
						qCoeff * imgDoublesLast[i * row + je] +
						qCoeff * imgDoublesLast[is * row + j] +
						qCoeff * imgDoublesLast[i * row + jw];

					imgDoublesNew[p] = *value;
				}
				else
				{
					int p = i * row + j * 4;

					value[0] = pCoeff * imgDoublesLast[i * row + j * 4] +
						qCoeff * imgDoublesLast[in * row + j * 4] +
						qCoeff * imgDoublesLast[i * row + je * 4] +
						qCoeff * imgDoublesLast[is * row + j * 4] +
						qCoeff * imgDoublesLast[i * row + jw * 4];

					value[1] = pCoeff * imgDoublesLast[i * row + j * 4 + 1] +
						qCoeff * imgDoublesLast[in * row + j * 4 + 1] +
						qCoeff * imgDoublesLast[i * row + je * 4 + 1] +
						qCoeff * imgDoublesLast[is * row + j * 4 + 1] +
						qCoeff * imgDoublesLast[i * row + jw * 4 + 1];

					value[2] = pCoeff * imgDoublesLast[i * row + j * 4 + 2] +
						qCoeff * imgDoublesLast[in * row + j * 4 + 2] +
						qCoeff * imgDoublesLast[i * row + je * 4 + 2] +
						qCoeff * imgDoublesLast[is * row + j * 4 + 2] +
						qCoeff * imgDoublesLast[i * row + jw * 4 + 2];

					imgDoublesNew[p] = value[0];
					imgDoublesNew[p+1] = value[1];
					imgDoublesNew[p+2] = value[2];

				}

			}
		}

		if (n % saveEvery == 0 || n == N - 1)
		{
			w->filterBufferAddImage(imgDoublesNew);
			std::cout << n << std::endl;
		}

		imgDoublesLast = imgDoublesNew;
		//imgDoublesNew.clear();

		delete[] value;
	}

	w->setHeatIsCalculated(true);
	std::cout << "done" << std::endl;

	return true;
}
bool ImageViewer::implicitLinearHeat(double tau, int N, int saveEvery, double omega)
{
	double h = 1;

	ViewerWidget* w = getCurrentViewerWidget();
	int depth = w->getImage()->depth();
	int row = w->getImage()->bytesPerLine();

	w->clearImageBuffer();

	// VLOZIM ORIGINALNY OBRAZOK
	std::vector<double> origin;
	uchar* originUCH = w->getData();
	for (int i = 0; i < w->height() * w->width(); i++)
	{
		origin.push_back(originUCH[i]);
	}
	w->filterBufferAddImage(origin);

	std::vector<double> uLast = w->getLastImg();
	std::vector<double> uNew = w->getLastImg();

	double pCoeff = 1.0 + tau / (h * h) * 4.0;
	double qCoeff = tau / (h * h);

	for (int n = 1; n < N; n++)
	{
		if (depth != 8)
		{
			return false;
		}

		uNew = LHE_SOR(uLast, pCoeff, qCoeff, w->width(), w->height(), row, omega);

		if (n % saveEvery == 0 || n == N - 1)
		{
			w->filterBufferAddImage(uNew);
			std::cout << n << std::endl;
		}

		uLast = uNew;
	}

	w->setHeatIsCalculated(true);

	return true;
}

std::vector<double> ImageViewer::LHE_SOR(std::vector<double> uLast, double pCoeff, double qCoeff, int width, int height, int row, double omega)
{
	int iter = 0;
	double stopKrit = 1;

	double value;

	std::vector<double> uNew = uLast;
	std::vector<double> uNewSOR(uNew.size(), 0.0);

	// SOR CYKLUS
	while (iter < 100 && stopKrit > pow(10, -6))
	{
		stopKrit = 0;

		if (iter != 0)
		{
			uNew = uNewSOR;
		}

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				int p = i * row + j;

				// INDEXY "MASKY" 
				int in = (i + 1 == height) ? i : i + 1;
				int is = (i - 1 == -1) ? 0 : i - 1;
				int je = (j + 1 == width) ? j : j + 1;
				int jw = (j - 1 == -1) ? 0 : j - 1;


				// SOR
				value = (1 - omega) * uNew[p] + (omega / pCoeff) * (uLast[p]
					+ qCoeff * (uNewSOR[is * row + j] + uNewSOR[i * row + jw])
					+ qCoeff * (uNew[in * row + j] + uNew[i * row + je]));

				// G-S
				//*value = (1 / pCoeff) * (uLast[p]
				//	+ qCoeff * (uNewSOR[is * row + j] + uNewSOR[i * row + jw])
				//	+ qCoeff * (uNew[in * row + j] + uNew[i * row + je]));

				// JACOBI
				//*value = (1 / pCoeff) * (uLast[p]
				//	+ qCoeff * (uNew[is * row + j] + uNew[i * row + jw] + uNew[in * row + j] + uNew[i * row + je]));

				uNewSOR[p] = value;


				stopKrit += fabs(pCoeff * uNewSOR[p] -
					qCoeff * (uNewSOR[is * row + j] + uNewSOR[i * row + jw] + uNew[in * row + j] + uNew[i * row + je]) - uLast[p]);
			}
		}

		stopKrit /= height * width;
		iter++;
	}

	std::cout << "iter = " << iter << std::endl;

	return uNewSOR;
}

// PERONA-MALIK
bool ImageViewer::explicitPeronaMalik(double tau, int N, int saveEvery, double k)
{
	double h = 1;

	ViewerWidget* w = getCurrentViewerWidget();
	int depth = w->getImage()->depth();
	int row = w->getImage()->bytesPerLine();

	if (depth != 8)
	{
		return false;
	}

	w->clearImageBuffer();

	// VLOZIM ORIGINALNY OBRAZOK
	std::vector<double> origin;
	uchar* originUCH = w->getData();
	for (int i = 0; i < w->height() * w->width(); i++)
	{
		origin.push_back(originUCH[i]);
	}
	w->filterBufferAddImage(origin);

	//std::cout << w->intensitySum() << std::endl;

	std::vector<double> imgDoublesLast = w->getLastImg();
	std::vector<double> imgDoublesNew(imgDoublesLast.size(), 0.0);

	for (int n = 0; n < N; n++)
	{
		double qCoeff = tau / (h * h);

		double value = 0.;

		for (int i = 0; i < w->getImgHeight(); i++)
		{

			for (int j = 0; j < w->getImgWidth(); j++)
			{
				//std::cout << i << " " << j << std::endl;

				int p = i * row + j;

				// INDEXY "MASKY" 
				int in = (i + 1 == w->getImgHeight()) ? i : i + 1;
				int is = (i - 1 == -1) ? 0 : i - 1;
				int je = (j + 1 == w->getImgWidth()) ? j : j + 1;
				int jw = (j - 1 == -1) ? 0 : j - 1;

				// GRADIENTY
				double gradE[2] =
				{
					imgDoublesLast[i * row + je] - imgDoublesLast[i * row + j],
					(imgDoublesLast[is * row + j] + imgDoublesLast[is * row + je] - imgDoublesLast[in * row + j] - imgDoublesLast[in * row + je]) / 4
				};
				double gradN[2] =
				{
					imgDoublesLast[is * row + j] - imgDoublesLast[i * row + j],
					(imgDoublesLast[is * row + jw] + imgDoublesLast[i * row + jw] - imgDoublesLast[is * row + je] - imgDoublesLast[i * row + je]) / 4
				};
				double gradW[2] =
				{
					imgDoublesLast[i * row + jw] - imgDoublesLast[i * row + j],
					(imgDoublesLast[in * row + jw] + imgDoublesLast[in * row + j] - imgDoublesLast[is * row + jw] - imgDoublesLast[is * row + j]) / 4
				};
				double gradS[2] =
				{
					imgDoublesLast[in * row + j] - imgDoublesLast[i * row + j],
					(imgDoublesLast[i * row + je] + imgDoublesLast[in * row + je] - imgDoublesLast[i * row + jw] - imgDoublesLast[in * row + jw]) / 4
				};

				//-------------------------------------------------------------------------------------------
				//double* gradN = gradInDir(imgDoublesLast, i, j, "N");
				//double* gradS = gradInDir(imgDoublesLast, i, j, "S");
				//double* gradE = gradInDir(imgDoublesLast, i, j, "E");
				//double* gradW = gradInDir(imgDoublesLast, i, j, "W");
				//-------------------------------------------------------------------------------------------

				double gn = g(norm(gradN, 2), k);
				double gs = g(norm(gradS, 2), k);
				double ge = g(norm(gradE, 2), k);
				double gw = g(norm(gradW, 2), k);

				//double gn = 1;
				//double gs = 1;
				//double ge = 1;
				//double gw = 1;

				double pCoeff = 1.0 - tau / (h * h) * (gn + gs + ge + gw);
				
				value = pCoeff * imgDoublesLast[i * row + j] +
					qCoeff * (gn * imgDoublesLast[in * row + j] +
							ge * imgDoublesLast[i * row + je] +
							gs * imgDoublesLast[is * row + j] +
							gw * imgDoublesLast[i * row + jw]);


				// hranovy detektor
				//imgDoublesNew[p] = (gn + gs + ge + gw) / 4 * 255;
				
				imgDoublesNew[p] = value;
			}
		}
		
		if (n % saveEvery == 0 || n == N - 1)
		{
			w->filterBufferAddImage(imgDoublesNew);
			std::cout << n << std::endl;
		}

		imgDoublesLast = imgDoublesNew;
		//imgDoublesNew.clear();
	}

	w->setHeatIsCalculated(true);
	std::cout << "done" << std::endl;

	return true;
}
bool ImageViewer::semi_implicitPeronaMalik(double tau, int N, int saveEvery, double omega, double k)
{
	double h = 1;

	ViewerWidget* w = getCurrentViewerWidget();
	int depth = w->getImage()->depth();
	int row = w->getImage()->bytesPerLine();

	if (depth == 8)
	{
		return false;
	}

	w->clearImageBuffer();

	// VLOZIM ORIGINALNY OBRAZOK
	std::vector<double> origin;
	uchar* originUCH = w->getData();
	for (int i = 0; i < w->height() * w->width(); i++)
	{
		origin.push_back(originUCH[i]);
	}
	w->filterBufferAddImage(origin);

	std::vector<double> uLast = w->getLastImg();
	std::vector<double> uSigma = w->getLastImg();
	std::vector<double> uNew = w->getLastImg();
	//std::vector<double> uNewSOR = w->getLastImg();
	std::vector<double> uNewSOR(uNew.size(), 0.0);

	for (int n = 1; n < N; n++)
	{
		double pCoeff = 1.0 + tau / (h * h) * 4.0;
		double qCoeff = tau / (h * h);

		double* value;
		if (depth == 8)
		{
			value = new double;
			*value = 0.;
		}
		else
		{
			value = new double[3];
			value[0] = value[1] = value[2] = 0.;
		}

		int iter = 0;
		double stopKrit = 1;

		// SOR CYKLUS
		while (iter < 100 && stopKrit > pow(10, -6))
		{
			stopKrit = 0;

			if (iter != 0)
			{
				uNew = uNewSOR;
			}

			for (int i = 0; i < w->getImgHeight(); i++)
			{
				for (int j = 0; j < w->getImgWidth(); j++)
				{
					int p = i * row + j;

					// INDEXY "MASKY" 
					int in = (i + 1 == w->getImgHeight()) ? i : i + 1;
					int is = (i - 1 == -1) ? 0 : i - 1;
					int je = (j + 1 == w->getImgWidth()) ? j : j + 1;
					int jw = (j - 1 == -1) ? 0 : j - 1;


					// SOR
					*value = (1 - omega) * uNew[p] + (omega / pCoeff) * (uLast[p]
						+ qCoeff * (uNewSOR[is * row + j] + uNewSOR[i * row + jw])
						+ qCoeff * (uNew[in * row + j] + uNew[i * row + je]));

					// G-S
					//*value = (1 / pCoeff) * (uLast[p]
					//	+ qCoeff * (uNewSOR[is * row + j] + uNewSOR[i * row + jw])
					//	+ qCoeff * (uNew[in * row + j] + uNew[i * row + je]));

					// JACOBI
					//*value = (1 / pCoeff) * (uLast[p]
					//	+ qCoeff * (uNew[is * row + j] + uNew[i * row + jw] + uNew[in * row + j] + uNew[i * row + je]));

					uNewSOR[p] = *value;
					


					stopKrit += fabs(pCoeff * uNewSOR[p] -
						qCoeff * (uNewSOR[is * row + j] + uNewSOR[i * row + jw] + uNew[in * row + j] + uNew[i * row + je]) - uLast[p]);
				}
			}

			stopKrit /= w->getImgHeight() * w->getImgWidth();
			iter++;
		}

		std::cout << "iter = " << iter << std::endl;

		if (n % saveEvery == 0 || n == N - 1)
		{
			w->filterBufferAddImage(uNewSOR);
			std::cout << n << std::endl;
		}

		uLast = uNew;
		uNewSOR = std::vector<double>(uNew.size(), 0.0);

		delete[] value;
	}

	w->setHeatIsCalculated(true);

	return true;

	return false;
}
double ImageViewer::g(double s, double K)
{
	return 1 / (1 + K * s * s);
}
double* ImageViewer::gradInDir(std::vector<double> u, int i, int j, std::string dir)
{
	ViewerWidget* w = getCurrentViewerWidget();
	int row = w->getImage()->bytesPerLine();

	double grad[2];

	int ip = (i + 1 == w->getImgHeight()) ? i : i + 1;
	int im = (i - 1 == -1) ? 0 : i - 1;
	int jp = (j + 1 == w->getImgWidth()) ? j : j + 1;
	int jm = (j - 1 == -1) ? 0 : j - 1;

	if (dir == "E")
	{
		grad[0] = u[i * row + jp] - u[i * row + j];
		grad[1] = (u[im * row + j] + u[im * row + jp] - u[ip * row + j] - u[ip * row + jp]) / 4;

		return grad;
	}
	else if (dir == "N")
	{
		grad[0] = u[im * row + j] - u[i * row + j];
		grad[1] = (u[im * row + jm] + u[i * row + jm] - u[im * row + jp] - u[i * row + jp]) / 4;

		return grad;
	}
	else if (dir == "W")
	{
		grad[0] = u[i * row + jm] - u[i * row + j];
		grad[1] = (u[ip * row + jm] + u[ip * row + j] - u[im * row + jm] - u[im * row + j]) / 4;

		return grad;
	}
	else if (dir == "S")
	{
		grad[0] = u[ip * row + j] - u[i * row + j];
		grad[1] = (u[i * row + jp] + u[ip * row + jp] - u[i * row + jm] - u[ip * row + jm]) / 4;

		return grad;
	}

	return nullptr;
}
double ImageViewer::norm(double* v, int dim)
{
	return (sqrt(v[0] * v[0] + v[1] * v[1]));

	double ret = 0.;
	for (int i = 0; i < dim; i++)
	{
		ret += v[i] * v[i];
	}
	return sqrt(ret);
}

bool ImageViewer::ISODATA()
{
	ViewerWidget* w = getCurrentViewerWidget();

	uchar* data = w->getData();

	int row = w->getImage()->bytesPerLine();
	int depth = w->getImage()->depth();

	if (depth != 8) return false;

	double treshold = w->isodataTreshold();

	for (int i = 0; i < w->getImgHeight(); i++)
	{
		for (int j = 0; j < w->getImgWidth(); j++)
		{
			if (depth == 8) {
				if (data[i * row + j] < treshold)
				{
					w->setPixel(w->getImage(), j, i, static_cast<uchar>(0));
				}
				else
				{
					w->setPixel(w->getImage(), j, i, static_cast<uchar>(255));
				}

			}
			else {
				return false;
				//uchar r = static_cast<uchar>(255 - data[i * row + j * 4]);
				//uchar g = static_cast<uchar>(255 - data[i * row + j * 4 + 1]);
				//uchar b = static_cast<uchar>(255 - data[i * row + j * 4 + 2]);
				//w->setPixel(w->getImage(), j, i, r, g, b);
			}
		}
	}
	w->update();

	return true;
}

#pragma endregion

#pragma region Tabs_slots

void ImageViewer::on_tabWidget_tabCloseRequested(int tabId)
{
	ViewerWidget* vW = getViewerWidget(tabId);
	vW->~ViewerWidget();
	ui->tabWidget->removeTab(tabId);
}
void ImageViewer::on_actionRename_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ViewerWidget* w = getCurrentViewerWidget();
	bool ok;
	QString text = QInputDialog::getText(this, QString("Rename"), tr("Image name:"), QLineEdit::Normal, w->getName(), &ok);
	if (ok && !text.trimmed().isEmpty())
	{
		w->setName(text);
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), text);
	}
}

#pragma endregion

#pragma region Image_slots

void ImageViewer::on_actionNew_triggered()
{
	newImgDialog = new NewImageDialog(this);
	connect(newImgDialog, SIGNAL(accepted()), this, SLOT(newImageAccepted()));
	newImgDialog->exec();
}
void ImageViewer::newImageAccepted()
{
	NewImageDialog* newImgDialog = static_cast<NewImageDialog*>(sender());

	int width = newImgDialog->getWidth();
	int height = newImgDialog->getHeight();
	QString name = newImgDialog->getName();
	openNewTabForImg(new ViewerWidget(name, QSize(width, height)));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);

	ui->time_Slider->setValue(0);
	ui->time_Slider->setEnabled(false);
	ui->linearHeatGroupBox->setVisible(false);

}
void ImageViewer::on_actionOpen_triggered()
{
	QString folder = settings.value("folder_img_load_path", "").toString();

	QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
	QString fileName = QFileDialog::getOpenFileName(this, "Load image", folder, fileFilter);
	if (fileName.isEmpty()) { return; }

	QFileInfo fi(fileName);
	settings.setValue("folder_img_load_path", fi.absoluteDir().absolutePath());

	if (!openImage(fileName)) {
		msgBox.setText("Unable to open image.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.exec();
	}

	ui->time_Slider->setValue(0);
	ui->time_Slider->setEnabled(false);
	ui->linearHeatGroupBox->setVisible(false);
}
void ImageViewer::on_actionSave_as_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image to save.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	QString folder = settings.value("folder_img_save_path", "").toString();

	ViewerWidget* w = getCurrentViewerWidget();

	QString fileFilter = "Image data (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm .*xbm .* xpm);;All files (*)";
	QString fileName = QFileDialog::getSaveFileName(this, "Save image", folder + "/" + w->getName(), fileFilter);
	if (fileName.isEmpty()) { return; }

	QFileInfo fi(fileName);
	settings.setValue("folder_img_save_path", fi.absoluteDir().absolutePath());

	if (!saveImage(fileName)) {
		msgBox.setText("Unable to save image.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.exec();
	}
	else {
		msgBox.setText(QString("File %1 saved.").arg(fileName));
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
	}
}
void ImageViewer::on_actionClear_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	clearImage();
}
void ImageViewer::on_actionInvert_colors_triggered() {
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	invertColors();
}
void ImageViewer::on_actionMakeBorder_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	makeBorders(100);
}
void ImageViewer::on_actionHistogram_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	ViewerWidget* w = getCurrentViewerWidget();

	w->calculateHistogram();
}
void ImageViewer::on_actionHistogram_stretch_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
	makeFullStretchedImage();
}
void ImageViewer::on_actionConvolution_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}

	QString folder = settings.value("folder_mask_load_path", "").toString();

	QString fileFilter = "Mask data (*.msk);;All files (*)";
	QString fileName = QFileDialog::getOpenFileName(this, "Load mask", folder, fileFilter);
	if (fileName.isEmpty()) { return; }

	QFileInfo fi(fileName);
	settings.setValue("folder_mask_load_path", fi.absoluteDir().absolutePath());

	if (!loadMask(fileName))
	{
		msgBox.setText("Unable to open mask.");
		msgBox.setIcon(QMessageBox::Warning);
		msgBox.exec();
	}
}
void ImageViewer::on_actionISODATA_2_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}

	if (!ISODATA())
	{
		msgBox.setText("Couldn't apply ISODATA.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}
}
void ImageViewer::on_actionBernsen_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}

	ViewerWidget* w = getCurrentViewerWidget();

	w->bernsenTreshold(15, 15, "bright");

	w->update();
}
void ImageViewer::on_actionLinear_Heat_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}

	ui->linearHeatGroupBox->setTitle("Linear Heat");

	ui->time_Slider->setValue(0);
	ui->linearHeatGroupBox->setVisible(true);
	ui->implicitRadioButton->setEnabled(true);
	ui->explicitRadioButton->setEnabled(true);
	ui->semiImplicitButton->setEnabled(false);
	ui->kSpinBox->setEnabled(false);


	filterType = filter_type::linear_heat;
}
void ImageViewer::on_actionPerona_Malik_triggered()
{
	if (!isImgOpened()) {
		msgBox.setText("No image is opened.");
		msgBox.setIcon(QMessageBox::Information);
		msgBox.exec();
		return;
	}

	ui->linearHeatGroupBox->setTitle("Perona-Malik");

	ui->time_Slider->setValue(0);
	ui->linearHeatGroupBox->setVisible(true);
	ui->implicitRadioButton->setEnabled(false);
	ui->explicitRadioButton->setEnabled(true);
	ui->semiImplicitButton->setEnabled(false);
	ui->kSpinBox->setEnabled(true);

	filterType = filter_type::Perona_Malik;
}

void ImageViewer::CalculateExplicitLinearHeat()
{
	ViewerWidget* w = getCurrentViewerWidget();

	if (w->isHeatCalculated())
	{
		QMessageBox msgBox;
		msgBox.setWindowTitle("Linear heat");
		msgBox.setText("Do you want to calculate from original image?");
		msgBox.setIcon(QMessageBox::Question);
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No| QMessageBox::Cancel);
		msgBox.setDefaultButton(QMessageBox::Yes);
		int ret = msgBox.exec();
		switch (ret)
		{
			case QMessageBox::Yes:
				w->setImageFromBuffer(0);
				break;
			case QMessageBox::No:
				w->setImageFromBuffer(ui->saveEvery_spinBox->value());
				break;
			case QMessageBox::Cancel:
				return;
		}
		//ui->time_Slider->setValue(0);
	}

	if (ui->explicitRadioButton->isChecked() && ui->explicitRadioButton->isEnabled())
	{
		if (filterType == filter_type::linear_heat)
		{
			explicitLinearHeat(ui->timeStep_SpinBox->value(), ui->numberOfTimeSteps_SpinBox->value(), ui->saveEvery_spinBox->value());
		}
		if (filterType == filter_type::Perona_Malik)
		{
			if (!explicitPeronaMalik(ui->timeStep_SpinBox->value(), ui->numberOfTimeSteps_SpinBox->value(), ui->saveEvery_spinBox->value(), ui->kSpinBox->value()))
			{
				msgBox.setText("Cannot apply Perona-Malik.");
				msgBox.setIcon(QMessageBox::Information);
				msgBox.exec();
				return;
			}
		}
	}
	else if (ui->implicitRadioButton->isChecked() && ui->implicitRadioButton->isEnabled())
	{
		if (filterType == filter_type::linear_heat)
		{
			if (!implicitLinearHeat(ui->timeStep_SpinBox->value(), ui->numberOfTimeSteps_SpinBox->value(), ui->saveEvery_spinBox->value(), ui->omegaSpinBox->value()))
			{
				msgBox.setText("Cannot apply LHE.");
				msgBox.setIcon(QMessageBox::Information);
				msgBox.exec();
				return;
			}
		}
	}

	ui->time_Slider->setValue(0);
	ui->time_Slider->setMaximum(ui->numberOfTimeSteps_SpinBox->value() / ui->saveEvery_spinBox->value() - 1);
	ui->time_Slider->setEnabled(true);
}
void ImageViewer::ChangeImageFromBuffer(int index)
{
	ViewerWidget* w = getCurrentViewerWidget();

	if (w->isHeatCalculated())
	{
		w->setImageFromBuffer(index);

		//std::cout << "Stredna intenzita: " << w->intensitySum() << std::endl;
		std::cout << "Stredna intenzita: " << w->intensitySumFromBuffer(index) / (w->width() * w->height()) << std::endl;

		w->update();
	}
}
void ImageViewer::HideLinearHeatGroupBox()
{
	ViewerWidget* w = getCurrentViewerWidget();

	if (w->isHeatCalculated())
	{
		QMessageBox msgBox;
		msgBox.setWindowTitle("Linear heat");
		msgBox.setText("Do you want to return to original image?");
		msgBox.setIcon(QMessageBox::Question);
		msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
		msgBox.setDefaultButton(QMessageBox::Yes);
		int ret = msgBox.exec();
		switch (ret)
		{
			case QMessageBox::Yes:
				w->setImageFromBuffer(0);
				break;
			case QMessageBox::No:
				//w->setImageFromBuffer(ui->saveEvery_spinBox->value());
				break;
			case QMessageBox::Cancel:
				return;
		}

		w->setHeatIsCalculated(false);
	}

	ui->time_Slider->setEnabled(false);
	ui->linearHeatGroupBox->setVisible(false);
}
void ImageViewer::ImageWidgetTabChanged(int index)
{
	ui->linearHeatGroupBox->setVisible(false);
}
void ImageViewer::ExplicitRadioButton_clicked()
{
	ui->omegaSpinBox->setEnabled(false);
}
void ImageViewer::ImplicitRadioButton_clicked()
{
	ui->omegaSpinBox->setEnabled(true);
}

#pragma endregion



