#pragma once
#include <QtWidgets>

class ViewerWidget :public QWidget {
	Q_OBJECT

public:
	ViewerWidget(QString viewerName, QSize imgSize, QWidget* parent = Q_NULLPTR);
	~ViewerWidget();
	void resizeWidget(QSize size);

	//Image functions
	bool setImage(const QImage& inputImg);	
	void setPixel(QImage * image, const int& x, const int& y, const uchar& r, const uchar& g, const uchar& b, const uchar& a = 255);
	void setPixel(QImage * image, const int& x, const int& y, const uchar& val);
	void setPixel(QImage * image, const int& x, const int& y, double& val);
	void setPixel(QImage * image, const int& x, const int& y, double& valR, double& valG, double& valB,const double& valA = 1.);
	double intensitySum();

	//Draw functions
	void freeDraw(const QPoint& end, const QPen& pen);

	//Class inline functions
	inline bool isEmpty() { if (img->size() == QSize(0, 0)) return true; else return false; };
	inline const QString& getName() { return name; }
	inline const void setName(QString newName) { name = newName; }
	inline int getImgWidth() { return img->width(); };
	inline int getImgHeight() { return img->height(); };
	inline void clear() {  };
	inline QImage* getImage() { return img; };
	inline uchar* getImageData() { return data; };
	inline bool isInside(QImage *image,int x, int y) 
		{ return (x >= 0 && y >= 0 && x < image->width() && y < image->height()) ? true : false; }
	inline void setPainter() { painter = new QPainter(img); }
	inline void setDataPtr() {	data = img->bits();}

	inline void setFreeDrawBegin(const QPoint& begin) { freeDrawBegin = begin; }
	inline const QPoint& getFreeDrawBegin() { freeDrawBegin; }
	inline void setFreeDrawActivated(bool state) { freeDrawActivated = state; }
	inline bool getFreeDrawActivated() { return freeDrawActivated; }

	inline uchar* getData() { return data; }

	// Buffer functions
	inline void filterBufferAddImage(std::vector<double> img) { filterImageBuffer.push_back(img); }
	inline std::vector<double> getImageFromBuffer(int index) { return filterImageBuffer[index]; }
	inline std::vector<double> getLastImg() { return filterImageBuffer[filterImageBuffer.size() - 1]; }
	inline void clearImageBuffer() { filterImageBuffer.clear(); }

	inline void setHeatIsCalculated(bool b) { heatCalculated = b; }
	inline bool isHeatCalculated() { return heatCalculated; }

	void setImageFromBuffer(int index);
	double intensitySumFromBuffer(int index);
	
	// Histogram functions
	inline int* getHistBlack() { return hist_black; }
	inline int* getHistRed() { return hist_red; }
	inline int* getHistGreen() { return hist_green; }
	inline int* getHistBlue() { return hist_blue; }
	inline bool isHistCalculated() { return histCalculated; }
	inline void HistCalculated() { histCalculated = true; }

	bool calculateHistogram();

	double isodataTreshold();
	void bernsenTreshold(int r, int cmin, std::string bg);

	int histogramCount(int a, int b);
	double histogramMean(int a, int b);

	void initializeHistBlack();
	void initializeHistColors();

	uchar Min(uchar* values);
	uchar Max(uchar* values);


private:
	QPointer<QTab> tab;
	QString name = "";
	QSize areaSize = QSize(0, 0);
	QImage* img = nullptr;
	uchar* data = nullptr;
	QPainter* painter = nullptr;

	bool freeDrawActivated = false;
	QPoint freeDrawBegin = QPoint(0, 0);

	int* hist_black = nullptr;
	int* hist_red = nullptr;
	int* hist_green = nullptr;
	int* hist_blue = nullptr;
	bool histCalculated = false;

	std::vector<std::vector<double>> filterImageBuffer;
	bool heatCalculated = false;

public slots:
	void paintEvent(QPaintEvent* event) Q_DECL_OVERRIDE;
};